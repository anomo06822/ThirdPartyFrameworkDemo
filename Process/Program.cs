﻿using System;
using ThirdPartyFrameworkDemo;
using ThirdPartyFrameworkDemo.AutoMapperDemo;
using ThirdPartyFrameworkDemo.Newtonsoft.Json;
using ThirdPartyFrameworkDemo.NF.EmitMapperDemo;

namespace Process
{
    class Program
    {
        static void Main()
        {
            Process();
        }

        private static void Process()
        {
            FakeDataFactory dataFactory = new FakeDataFactory();
            var context = dataFactory.Create();

            BaseServiceProcessor[] serviceProcessors = new BaseServiceProcessor[]
            { new AutoMapperProcessor(), new EmitMapperProcessor()};

            foreach(var serviceProcessor in serviceProcessors)
            {
                var name = serviceProcessor.GetType();
                Console.WriteLine(FormatProcessor(name, "Process"));
                serviceProcessor.Process(context);
            }

            Console.WriteLine(JsonConvertHelper.SerializeObject(context));
            Console.WriteLine("Process end");
        }

        private static string FormatProcessor(Type name, string status)
        {
            return string.Format("{0} {1}", name, status);
        }
    }
}
