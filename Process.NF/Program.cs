﻿using System;
using System.Diagnostics;
using ThirdPartyFrameworkDemo;
using ThirdPartyFrameworkDemo.Model;
using ThirdPartyFrameworkDemo.NF.AutoMapperDemo;
using ThirdPartyFrameworkDemo.NF.EmitMapperDemo;
using ThirdPartyFrameworkDemo.NF.Newtonsoft.Json;

namespace Process.NF
{
    class Program
    {
        public static int runTimeCount = 1000;

        static void Main()
        {
            Process();
        }

        private static void Process()
        {
            FakeDataFactory dataFactory = new FakeDataFactory();
            var context = dataFactory.Create();

            BaseServiceProcessor[] serviceProcessors = new BaseServiceProcessor[]
            { new AutoMapperProcessor() ,new EmitMapperProcessor()};

            Stopwatch sw = new Stopwatch();

            foreach (var serviceProcessor in serviceProcessors)
            {
                var name = serviceProcessor.GetType();
                Console.WriteLine(FormatProcessor(name, "Process"));
                sw.Reset();
                sw.Start();
                ProcessRunTime(context, serviceProcessor, runTimeCount);
                sw.Stop();
                Console.WriteLine(FormatProcessorTime(name, sw.ElapsedMilliseconds));
            }

            Console.WriteLine(JsonConvertHelper.SerializeObject(context));
            Console.WriteLine("\n");
            Console.WriteLine("please input any key, and then process will be closed!");
            Console.ReadKey();
        }

        private static void ProcessRunTime(ServiceContext context, BaseServiceProcessor serviceProcessor, int count)
        {
            for (int i = 0; i < count; i++)
            {
                serviceProcessor.Process(context);
            }
        }

        private static string FormatProcessor(Type name, string status)
        {
            return string.Format("{0} {1}", name, status);
        }

        private static string FormatProcessorTime(Type name, long time)
        {
            return string.Format("{0}, time : {1}", name, time);
        }

    }
}

