﻿using AutoMapper;
using ThirdPartyFrameworkDemo.Model;

namespace ThirdPartyFrameworkDemo.AutoMapperDemo
{
    public class ItemMappingProfile : Profile
    {
        public ItemMappingProfile()
        {
            CreateMap<Item, FakeItem>().ReverseMap();
        }
    }
}
