﻿using System;
using System.Collections.Generic;
using System.Text;
using ThirdPartyFrameworkDemo.Model;

namespace ThirdPartyFrameworkDemo.AutoMapperDemo
{
    public class AutoMapperProcessor : BaseServiceProcessor
    {
        public override void Process(ServiceContext context)
        {
            var config = new AutoMapperConfiguration().Configure();
            var iMapper = config.CreateMapper();

            context.MapperContext = new MapperContext()
            {
                Item = new ItemTransfer().MapItem(context.MapperContext.FakeItem)
            };
        }
    }
}
