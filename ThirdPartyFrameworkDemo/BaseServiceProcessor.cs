﻿using System;
using System.Collections.Generic;
using System.Text;
using ThirdPartyFrameworkDemo.Model;

namespace ThirdPartyFrameworkDemo
{
    public abstract class BaseServiceProcessor
    {
        public abstract void Process(ServiceContext context);
    }
}
