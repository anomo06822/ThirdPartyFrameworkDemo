﻿namespace ThirdPartyFrameworkDemo.Model
{
    public class DetailInfo
    {
        public string Description { get; set; }

        public string ImageUrl { get; set; }
    }
}