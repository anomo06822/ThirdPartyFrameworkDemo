﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ThirdPartyFrameworkDemo.Model
{
    public class Item
    {
        public string ItemNumber { get; set; }

        public DetailInfo DetailInfo { get; set; }

        public string ItemKey
        {
            get
            {
                return ItemNumber;
            }
        }
    }
}
