﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ThirdPartyFrameworkDemo.Model
{
    public class FakeItem
    {
        public string ItemNumber { get; set; }

        public FakeDetailInfo DetailInfo { get; set; }

        public string ItemKey
        {
            get
            {
                return ItemNumber;
            }
        }
    }
}
