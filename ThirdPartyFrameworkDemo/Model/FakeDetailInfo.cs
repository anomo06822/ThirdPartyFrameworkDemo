﻿namespace ThirdPartyFrameworkDemo.Model
{
    public class FakeDetailInfo
    {
        public string Description { get; set; }

        public string ImageUrl { get; set; }
    }
}