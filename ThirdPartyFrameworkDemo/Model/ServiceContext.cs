﻿namespace ThirdPartyFrameworkDemo.Model
{
    public class ServiceContext
    {
        public MapperContext MapperContext { get; set; }

        public MapperContext AutoMapperContext { get; set; }

        public MapperContext EmitMapperContext { get; set; }
    }
}
