﻿using System;
using System.Collections.Generic;
using System.Text;
using ThirdPartyFrameworkDemo.Model;

namespace ThirdPartyFrameworkDemo
{
    public class FakeDataFactory
    {
        public ServiceContext Create()
        {
            ServiceContext context = new ServiceContext
            {
                MapperContext = CreateMapperContext()
            };

            return context;
        }
        
        private MapperContext CreateMapperContext()
        {
            MapperContext mapperContext = new MapperContext
            {
                FakeItem = new FakeItem()
                {
                    ItemNumber = "XX-XXX-XXXX",
                    DetailInfo = new FakeDetailInfo()
                    {
                        Description = "HTC UU",

                        ImageUrl = "imageUrl"
                    },
                }
            };

            return mapperContext;
        }
    }
}
