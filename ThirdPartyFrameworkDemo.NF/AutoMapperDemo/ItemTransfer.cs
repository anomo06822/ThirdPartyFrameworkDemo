﻿using AutoMapper;
using ThirdPartyFrameworkDemo.Model;

namespace ThirdPartyFrameworkDemo.NF.AutoMapperDemo
{
    public class ItemTransfer
    {
        private static MapperConfiguration config = new AutoMapperConfiguration().Configure();
        private static IMapper iMapper = config.CreateMapper();

        public Item MapItem(FakeItem sourceItem)
        {
            if (sourceItem != null)
            {
                return iMapper.Map<Item>(sourceItem);
            }

            return null;
        }
    }
}
