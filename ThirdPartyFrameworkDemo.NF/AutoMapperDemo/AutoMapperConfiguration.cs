﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace ThirdPartyFrameworkDemo.NF.AutoMapperDemo
{
    public class AutoMapperConfiguration
    {
        public MapperConfiguration Configure()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ItemMappingProfile>();
            });

            return config;
        }
    }
}
