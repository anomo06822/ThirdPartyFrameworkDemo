﻿using AutoMapper;
using ThirdPartyFrameworkDemo.Model;

namespace ThirdPartyFrameworkDemo.NF.AutoMapperDemo
{
    public class ItemMappingProfile : Profile
    {
        public ItemMappingProfile()
        {
            CreateMap<Item, FakeItem>().ReverseMap();
        }
    }
}
