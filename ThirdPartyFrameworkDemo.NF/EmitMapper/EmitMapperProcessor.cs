﻿using EmitMapper;
using ThirdPartyFrameworkDemo.Model;

namespace ThirdPartyFrameworkDemo.NF.EmitMapperDemo
{
    public class EmitMapperProcessor : BaseServiceProcessor
    {
        public override void Process(ServiceContext context)
        {
            var mapper = ObjectMapperManager
                        .DefaultInstance
                        .GetMapper<FakeItem, Item>();

            context.EmitMapperContext = new MapperContext()
            {
                Item = mapper.Map(context.MapperContext.FakeItem)
            };
        }
    }
}
