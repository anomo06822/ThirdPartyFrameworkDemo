﻿using Newtonsoft.Json;

namespace ThirdPartyFrameworkDemo.NF.Newtonsoft.Json
{
    public class JsonConvertHelper
    {
        public static T DeserializeObject<T>(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return default(T);
            }

            return JsonConvert.DeserializeObject<T>(text);
        }

        public static string SerializeObject(object obj)
        {
            if (obj == null)
            {
                return null; 
            }

            return JsonConvert.SerializeObject(obj);
        }
    }
}
